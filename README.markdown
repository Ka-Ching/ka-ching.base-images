# Ka-Ching.Base-Images

Base images for [Ka-Ching.Auto-Builds](
    https://gitlab.com/Ka-Ching/ka-ching.autobuilds
).

## Status

[![pipeline status](https://gitlab.com/Ka-Ching/ka-ching.base-images/badges/master/pipeline.svg)](https://gitlab.com/Ka-Ching/ka-ching.base-images/-/commits/master)

## Schedule

The builds run once a week, on Sunday at 2 am UTC.

## License

The content of this project's repository are licensed as GPLv3.
